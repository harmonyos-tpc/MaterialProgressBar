# MaterialProgressBar

  MaterialProgressBar : MaterialProgressBar with consistent appearance on UI.

# MaterialProgressBar includes :
* Correct tinting.
* Able to hide the progress background of framework horizontal ProgressBar.
* Able to show a determinate round progress.
* Used as a drop-in replacement for framework ProgressBar.

# Usage Instructions
1.	You can either simply use the widget, or set elements from this library on a regular ProgressBar.
2.	Using Widget
Simply replace your ProgressBar with MaterialProgressBar, and remember to apply corresponding attribute for correct behavior.
For example, to create an indeterminate horizontal MaterialProgressBar:

   <me.zhanghai.harmony.materialprogressbar.MaterialDeterminateProgressBar
                ohos:width="105vp"
                ohos:id="$+id:normalStyleProgressBar"
                ohos:height="105vp"
                ohos:margin="10vp"
                ohos:min_height="50vp"
                ohos:max_height="50vp"
                ohos:min_width="50vp"
                ohos:max_width="50vp"
                ohos:progress_width="5vp"
                app:progressbar_style="normal"
                app:progressbar_type="primary"
                app:progressbar_tinted="false"/>

Available custom attributes:
app:progressbar_style: Style of progress element: dynamic or normal.
app:progressbar_type: Type of progressBar is Primary progressbar or vice progressbar.
app:progressbar_tinted: To tint the progress on progressbar. It can be true or false.
3.	Using Arc
For example, to set a determinateRoundProgressDrawable on a ProgressBar.
arc.setArc(SWEEP_ANGLE, normalProgressStatus, false);
       arcSecondary.setArc(SWEEP_ANGLE, normalProgressStatus, false);
You will also need to set a style from this library as in the section above.
Don't forget to create and set the element as above.


# Installation Instructions

1.For using MaterialProgressBar module in sample app, Add the dependencies in entry/build.gradle as below :

		dependencies {
			implementation project(path: ':library')
		}

2.Using the library.har, make sure to add library.har file in the entry/libs folder.

	Modify the dependencies in the entry/build.gradle file.
		dependencies {
			implementation fileTree(dir: 'libs', include: [' *.jar', ' *.har'])
		}

3. For using MaterialProgressBar from a remote repository in separate application, add the below dependencies  :

    	Modify entry build.gradle as below :
    	```
    	dependencies {
    	    implementation 'io.openharmony.tpc.thirdlib:MaterialProgressBar:1.0.0'
    	}
        ```
		
# License

Copyright 2015 Hai Zhang


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at


http://www.apache.org/licenses/LICENSE-2.0


Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.