/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.zhanghai.harmony.materialprogressbar.sample.slice;


import me.zhanghai.harmony.materialprogressbar.MaterialRoundProgressBar;
import me.zhanghai.harmony.materialprogressbar.sample.ResourceTable;
import me.zhanghai.harmony.materialprogressbar.util.LogUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * MainAbilitySlice extends AbilitySlice
 *
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    /**
     * RoundProgressBar instance roundProgressBar
     */
    private static final String TAG = MainAbilitySlice.class.getName();
    private static final String KEY = "value";
    private static final String VALUE = "a";
    private static final int WHITE = 0xffffffff;
    private static final int CYAN = 0xFF41C7D6;
    private static final int MAX_PROGRESS = 100;
    private static final int ONE = 1;
    private final EventHandler mHandler = new EventHandler(EventRunner.getMainEventRunner());
    private int status = 0;
    private MaterialRoundProgressBar normalStyleProgressBar;
    private MaterialRoundProgressBar normalStyleProgressBarSecondary;
    private MaterialRoundProgressBar normalStyleProgressBarBackground;
    private Text tvRounded;
    private ComponentContainer view;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        if (LayoutScatter.getInstance(this).parse(ResourceTable.Layout_ability_main, null, false) instanceof ComponentContainer) {
            view = (ComponentContainer) LayoutScatter.getInstance(this).parse(ResourceTable.Layout_ability_main, null, false);
        }
        if (view.findComponentById(ResourceTable.Id_tvRounded) instanceof Text) {
            tvRounded = (Text) view.findComponentById(ResourceTable.Id_tvRounded);
        }
        setUIContent(view);
        initRoundProgressBar();
    }

    private void initRoundProgressBar() {
        ShapeElement normal = new ShapeElement();
        ShapeElement dynamic = new ShapeElement();
        dynamic.setShape(ShapeElement.PATH);
        normal.setRgbColor(RgbColor.fromArgbInt(WHITE));
        dynamic.setRgbColor(RgbColor.fromArgbInt(CYAN));

        if (view.findComponentById(ResourceTable.Id_normalStyleProgressBar) instanceof MaterialRoundProgressBar) {
            normalStyleProgressBar = (MaterialRoundProgressBar) view.findComponentById(ResourceTable.Id_normalStyleProgressBar);
            normalStyleProgressBar.setProgressBackgroundElement(normal);
        }
        if (view.findComponentById(ResourceTable.Id_normalStyleProgressBarSecondary) instanceof MaterialRoundProgressBar) {
            normalStyleProgressBarSecondary = (MaterialRoundProgressBar) view.findComponentById(ResourceTable.Id_normalStyleProgressBarSecondary);
            normalStyleProgressBarSecondary.setProgressBackgroundElement(normal);
        }
        if (view.findComponentById(ResourceTable.Id_normalStyleProgressBarBackground)
                instanceof MaterialRoundProgressBar) {
            normalStyleProgressBarBackground = (MaterialRoundProgressBar) view.findComponentById(ResourceTable.Id_normalStyleProgressBarBackground);
        }
        showProgressBar();
    }

    @Override
    public void onActive() {
        super.onActive();
        tvRounded.setClickedListener(this);
    }

    @Override
    public void onInactive() {
        super.onInactive();
    }

    private void showProgressBar() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (status < MAX_PROGRESS) {
                    status += ONE;
                    mHandler.postTask(new Runnable() {
                        @Override
                        public void run() {
                            normalStyleProgressBar.setProgressValue(status);
                            normalStyleProgressBarSecondary.setProgressValue(status);
                            normalStyleProgressBarBackground.setProgressValue(status);
                        }
                    });
                    try {
                        Thread.sleep(MAX_PROGRESS);
                    } catch (InterruptedException e) {
                        LogUtil.error(TAG, "InterruptedException");
                    }
                }
            }
        }).start();
    }

    @Override
    public void onClick(Component component) {
        if (component.getId() == ResourceTable.Id_tvRounded) {
            AbilitySlice targetSlice = new DeterminateRoundSampleSlice();
            Intent intent = new Intent();
            intent.setParam(KEY, VALUE);
            present(targetSlice, intent);
        }
    }
}