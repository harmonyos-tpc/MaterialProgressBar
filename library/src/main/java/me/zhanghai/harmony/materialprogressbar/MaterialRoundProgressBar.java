/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.zhanghai.harmony.materialprogressbar;

import ohos.agp.components.AttrSet;
import ohos.agp.components.RoundProgressBar;
import ohos.app.Context;

/**
 * MaterialRoundProgressBar
 */
public class MaterialRoundProgressBar extends RoundProgressBar {

    /**
     * Constructor.
     *
     * @param context used context for reference, pass to base class.
     */
    public MaterialRoundProgressBar(Context context) {
        super(context);
    }

    /**
     * Constructor.
     * *
     *
     * @param context is used context for reference, pass to base class.
     * @param attrSet is used to pass the set of attributes to the base class.
     */
    public MaterialRoundProgressBar(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    /**
     * Constructor.
     *
     * @param context   used context for reference, pass to base class with attributeset and stylename.
     * @param attrSet   is used to pass the set of attributes to the base class.
     * @param styleName is used to pass the set of attributes to the base class.
     */
    public MaterialRoundProgressBar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }
}