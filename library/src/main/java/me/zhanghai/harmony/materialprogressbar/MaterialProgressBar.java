/*
 * Copyright (c) 2015 Zhang Hai <Dreaming.in.Code.ZH@Gmail.com>
 * All Rights Reserved.
 */

package me.zhanghai.harmony.materialprogressbar;

import ohos.agp.components.AttrSet;
import ohos.agp.components.ProgressBar;
import ohos.agp.components.element.Element;
import ohos.app.Context;

/**
 * MaterialProgressBar
 */
public class MaterialProgressBar extends ProgressBar {
    /**
     * integer Variable PROGRESS_STYLE_CIRCULAR
     */
    public static final int PROGRESS_STYLE_CIRCULAR = 0;
    /**
     * integer Variable PROGRESS_STYLE_HORIZONTAL
     */
    public static final int PROGRESS_STYLE_HORIZONTAL = 1;
    /**
     * integer Variable DURATION
     */
    private static final int DETERMINATE_CIRCULAR_PROGRESS_STYLE_NORMAL = 0;
    /**
     * integer Variable DETERMINATE_CIRCULAR_PROGRESS_STYLE_DYNAMIC
     */
    private static final int DETERMINATE_CIRCULAR_PROGRESS_STYLE_DYNAMIC = 1;

    /**
     * boolean Variable mSuperInitialized
     */
    private boolean mSuperInitialized = true;

    /**
     * integer Variable mProgressStyle
     */
    private int mProgressStyle;

    /**
     * Constructor.
     *
     * @param context used context for reference, pass to base class.
     */
    public MaterialProgressBar(Context context) {
        super(context);
    }

    /**
     * Constructor.
     *
     *
     * @param context is used context for reference, pass to base class.
     * @param attrSet is used to pass the set of attributes to the base class.
     */
    public MaterialProgressBar(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    /**
     * Constructor.
     *
     * @param context   used context for reference, pass to base class with attributeset and stylename.
     * @param attrSet   is used to pass the set of attributes to the base class.
     * @param styleName is used to pass the set of attributes to the base class.
     */
    public MaterialProgressBar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    /**
     * method to get current element
     *
     * @return return Element object to set on ProgressBar.
     */
    private Element getCurrentElement() {
        return isInfiniteMode() ? getInfiniteModeElement() : getProgressElement();
    }
}